module shadow_recorder

go 1.14

require (
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/youpy/go-riff v0.0.0-20131220112943-557d78c11efb // indirect
	github.com/youpy/go-wav v0.0.0-20160223082350-b63a9887d320
)
