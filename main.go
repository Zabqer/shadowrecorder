package main

import (
	"encoding/binary"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gordonklaus/portaudio"
	"github.com/youpy/go-wav"
)

var (
	SampleRate     = 44100
	BytesPerSample = 2
	Seconds        = 10
	saveFolder     string
)

var (
	buffer []int16
	index  = 0
)

func getSaveFilename() string {
	return fmt.Sprintf(
		"%s/%s.wav",
		saveFolder,
		time.Now().Format("2006_01_02_15_04_05"),
	)
}

func saveBuffer() {
	fmt.Println("Hotkey recived. Saving buffer...")
	file, err := os.Create(getSaveFilename())
	if err != nil {
		panic(err)
	}
	writer := wav.NewWriter(file, uint32(len(buffer)), 2, uint32(SampleRate), uint16(8*BytesPerSample))
	binary.Write(writer, binary.LittleEndian, buffer[index:])
	binary.Write(writer, binary.LittleEndian, buffer[:index])
	file.Close()
	fmt.Println("Saved")
}

func main() {
	flag.StringVar(&saveFolder, "save_folder", os.Getenv("HOME")+"/ShadowRecords", "Absolute path to folder for saving records")
	flag.IntVar(&SampleRate, "sample_rate", 44100, "Record sample rate")
	flag.IntVar(&Seconds, "seconds", 10, "Seconds to record")

	flag.Parse()

	buffer = make([]int16, SampleRate*BytesPerSample*Seconds)

	portaudio.Initialize()
	defer portaudio.Terminate()

	in := make([]int16, SampleRate*BytesPerSample)
	stream, err := portaudio.OpenDefaultStream(2, 0, float64(SampleRate), len(in), in)
	if err != nil {
		panic(err)
	}
	defer stream.Close()

	stream.Start()

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

	save := make(chan os.Signal, 1)
	signal.Notify(save, syscall.SIGUSR1)

	for {
		err := stream.Read()
		if err != nil {
			panic(err)
		}
		for i := 0; i < SampleRate*BytesPerSample; i++ {
			buffer[index+i] = in[i]
		}
		index += SampleRate * BytesPerSample
		if index >= SampleRate*BytesPerSample*Seconds {
			index = 0
		}
		select {
		case <-exit:
			stream.Stop()
			return
		case <-save:
			saveBuffer()
		default:
		}
	}
}
